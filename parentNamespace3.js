/// <reference path="parentNamespace1.ts" />
/// <reference path="parentNamespace2.ts" />
var parentNamespace;
(function (parentNamespace) {
    console.log("@file 3 - log 1   childNamespace : " + parentNamespace.childNamespace + " // [object Object]");
    console.log("@file 3 - log 2   childNamespace.signal : " + parentNamespace.childNamespace.signal + " // function body");
    console.log("@file 3 - exportedPing : " + parentNamespace.exportedPing
        + "  // 'pong' -  we do not have to use the namespace : this works");
    console.log("@file 3 - parentNamespace.exportedPing : " + parentNamespace.exportedPing
        + "  // 'pong'  - we may use the namespace : this works too");
    console.log("@file 3 -  parentNamespace.declaredPing : " + parentNamespace.declaredPing + "  // 'foo'");
    console.log("@file 3 -  parentNamespace.handleForChildExports : " + parentNamespace.handleForChildExports + "  // [object Object]");
    console.log("@file 3 -  parentNamespace.handleForChildExports : " + parentNamespace.handleForChildExports.signal + "  // function body");
    console.log(" ");
    parentNamespace.handleForChildExports.signal(parentNamespace.handleForChildExports.declaredPing);
    // parentNamespace.handleForChildExports = void(0);
    console.log(" ");
    parentNamespace.childNamespace.signal(" * * * @file 3 : from parentNamespace : handleForChildExports was deleted, but childNamespace wqas not");
    // not shared because not exported : parentNamespace.localChildNamespace.warning(" * * * @file 3 : parentNamespace.localChildNamespace.warning");
    console.log(" ");
    console.log("@file 3 - DELETED HANDLE - exportedPing : " + parentNamespace.exportedPing + "  // 'pong'");
    console.log("@file 3 - DELETED HANDLE - parentNamespace.exportedPing : " + parentNamespace.exportedPing + "  // 'pong'");
    console.log("@file 3 - DELETED HANDLE -  parentNamespace.declaredPing : " + parentNamespace.declaredPing + "  // 'foo'");
    console.log("@file 3 - DELETED HANDLE -  parentNamespace.handleForChildExports : " + parentNamespace.handleForChildExports + "  // undefined");
    console.log(" ");
    console.log("@file 3 - DELETED HANDLE - log 1   childNamespace : " + parentNamespace.childNamespace + " // [object Object]");
    console.log("@file 3 - DELETED HANDLE - log 2   childNamespace.signal : " + parentNamespace.childNamespace.signal + " // function body");
})(parentNamespace || (parentNamespace = {}));
parentNamespace.childNamespace.signal(" * * * @file 3 : from global namespace : parentNamespace.childNamespace.signal : handleForChildExports was deleted, but childNamespace wqas not");
console.log("@file 3 - log  a : " + parentNamespace + "  // [object Object]");
console.log("@file 3 - log  b : " + parentNamespace.childNameSpace + "  // [object Object]");
console.log("@file 3 - log  c : " + parentNamespace.exportedPing + "  // pong");
console.log("@file 3 - log  d : " + parentNamespace.declaredPing + "  // foo");
console.log("@file 3 - log  e : " + parentNamespace.handleForChildExports + "  // undefined");
console.log(" ");
//# sourceMappingURL=parentNamespace3.js.map