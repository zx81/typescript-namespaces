/// <reference path="parentNamespace1.ts" />
namespace parentNamespace {
    export namespace childNamespace {
        export function signal(a): void { // protected after NS deletion
            console.log('signal exported from childNamespace logs : ' + a);
        }
        console.log(" ");
        signal("* * * @file 2 : from childNamespace");
        console.log(" ");
        function internal(a): void { // private
            console.log('signal exported from childNamespace logs : ' + a);
        }
    }
    namespace localChildNamespace{
        export function warning(a): void { // protected after NS deletion
            console.warn('warning exported from childNamespace logs : ' + a);
        }
    }
    childNamespace.declaredPing = "dubigeau"; // added as member directly to the namespace object, same effect as a export, protected after NS deletion

    localChildNamespace.warning(" * * * @file 2 : localChildNamespace.warning");
    console.log("@file 2 - log 1   childNamespace : " + childNamespace + " // [object Object]" );
    console.log("@file 2 - log 2   childNamespace.signal : " + childNamespace.signal + " // function body ");
    console.log("@file 2 - log 2   childNamespace.internal : " + childNamespace.internal + " // undefined ");

    export var handleForChildExports = childNamespace; // the name of the namespace cannot be the same as the name of the namesapce


    console.log("@file 2 - log  I    handleForChildExports : " + handleForChildExports  + "  // [object Object]");
    console.log("@file 2 - log  II   handleForChildExports.signal : " + handleForChildExports.signal  + "     // function body");
    // Error : not defined :console.log("@file 2 - log  III  declaredPing : " + declaredPing + "  // 'foo'"); // localPing was declared and exported afterwards
    console.log("@file 2 - log  IV   exportedPing : " + exportedPing  + " // 'pong'"); // localPing was declared in export statement
    // Error : not defined : console.log("@file 2 - log  V    localPing : " + localPing  + " // undefined"); // localPing was declared but not exported
    console.log(" ");

    console.log("@file 2 - log  i    parentNamespace.handleForChildExports : " + parentNamespace.handleForChildExports  + "  // [object Object]");
    console.log("@file 2 - log  ii   parentNamespace.handleForChildExports.signal : " + parentNamespace.handleForChildExports.signal  + "     // function body");
    console.log("@file 2 - log  iii  parentNamespace.declaredPing : " + parentNamespace.declaredPing + "  // 'foo'"); // localPing was declared and exported afterwards
    console.log("@file 2 - log  iv   parentNamespace.exportedPing : " + parentNamespace.exportedPing  + " // 'pong'"); // localPing was declared in export statement
    console.log("@file 2 - log  v    parentNamespace.localPing : " + parentNamespace.localPing  + " // undefined"); // localPing was declared but not exported
    console.log(" ");
    parentNamespace.childNamespace.signal("* * * @file 2 : from  parentNamespace");
    console.log(" ");
}
parentNamespace.childNamespace.signal("* * * @file 2 : from global namespace : parentNamespace.childNamespace.signal");
// parentNamespace.localChildNamespace.warning(" * * * @file 2 : from global namespace : parentNamespace.localChildNamespace.warning");

console.log("@file 3 - log  a : " + parentNamespace + "  // [object Object]" );
console.log("@file 3 - log  b : " + parentNamespace.childNameSpace + "  // [object Object]" );
console.log("@file 2 - log  b   parentNamespace.handleForChildExports : " + parentNamespace.handleForChildExports  + "  // [object Object]");
console.log("@file 2 - log  c   parentNamespace.handleForChildExports.signal : " + parentNamespace.handleForChildExports.signal  + "  // function body");
console.log("@file 2 - log  d   parentNamespace.declaredPing : " + parentNamespace.declaredPing + "  // 'foo'");
console.log("@file 2 - log  e   parentNamespace.exportedPing : " + parentNamespace.exportedPing + "  // 'pong'");
console.log("@file 2 - log  f   parentNamespace.localPing : " + parentNamespace.localPing + "  // undefined");
console.log(" ");
// Error : not defined : console.log("@file 2 - log  x   parentNamespace.exportedPing : " + exportedPing + "  // 'pong'");


