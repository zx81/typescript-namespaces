namespace parentNamespace {
    export const exportedPing = "pong"; // public between separated files, no prefix needed
    const declaredPing = "foo";
    parentNamespace.declaredPing = declaredPing; // public between separated files through parentNamespace prefix
    const localPing = "fuckTrump" // private between separated files
}
