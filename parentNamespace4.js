/// <reference path="parentNamespace1.ts" />
/// <reference path="parentNamespace2.ts" />
/// <reference path="parentNamespace3.ts" />
// namespace parentNamespace {
// parentNamespace = void (0);
// Error : not defined : console.log("@file 4 : test 1 - DELETED PARENTNAMESPACE - exportedPing : " + exportedPing + "  // 'pong' -  we do not have to use the namespace : this works");
// Error : not defined : console.log("@file 4 - DELETED PARENTNAMESPACE - parentNamespace.exportedPing : " + parentNamespace.exportedPing + "  // 'pong'  - we may use the namespace : this works too");
// Error : not defined : console.log("@file 4 : test 2 - DELETED PARENTNAMESPACE -  parentNamespace.declaredPing : " + parentNamespace.declaredPing + "  // 'foo'"); console.log("@file 4 : test 3 - DELETED PARENTNAMESPACE -  parentNamespace.handleForChildExports : " + parentNamespace.handleForChildExports + "  // undefined");
// Error : not defined :  parentNamespace.childNamespace.signal(" * * * @file 4 : PARENTNAMESPACE DELETED IN PARENTNAMESPACE BUT NOT IN GLOBAL")
console.log(" ");
parentNamespace.childNamespace.signal(" * * * @file 4 : PARENTNAMESPACE DELETED IN PARENTNAMESPACE BUT NOT IN GLOBAL");
console.log("@file 4 : test a - PARENTNAMESPACE DELETED IN PARENTNAMESPACE BUT NOT IN GLOBAL - log  a parentNamespace : " + parentNamespace + "  // [object Object]");
console.log("@file 4 : test b - PARENTNAMESPACE DELETED IN PARENTNAMESPACE BUT NOT IN GLOBAL - log  b : " + parentNamespace.exportedPing + "  // pong");
console.log("@file 4 : test c - PARENTNAMESPACE DELETED IN PARENTNAMESPACE BUT NOT IN GLOBAL - log  c : " + parentNamespace.declaredPing + "  // foo");
console.log("@file 4 : test d - PARENTNAMESPACE DELETED IN PARENTNAMESPACE BUT NOT IN GLOBAL - log  d : " + parentNamespace.handleForChildExports + "  // undefined");
console.log(" ");
console.log("@file 3 - PARENTNAMESPACE DELETED IN PARENTNAMESPACE BUT NOT IN GLOBAL - log 1   childNamespace : " + parentNamespace.childNamespace + " // [object Object]");
console.log("@file 3 - PARENTNAMESPACE DELETED IN PARENTNAMESPACE BUT NOT IN GLOBAL - log 2   childNamespace.signal : " + parentNamespace.childNamespace.signal + " // function body");
console.log(" ");
// parentNamespace = void(0);
console.log("@file 4 : test a2 - DELETED PARENTNAMESPACE FROM GLOBAL - log  a  parentNamespace : " + parentNamespace + "  // undefined");
// Error : not defined :  console.log("@file 4 : test b - DELETED PARENTNAMESPACE FROM GLOBAL - log  b : " + parentNamespace.exportedPing + "  // pong");
// Error : not defined :  console.log("@file 4 : test c - DELETED PARENTNAMESPACE FROM GLOBAL - log  c : " + parentNamespace.declaredPing + "  // foo");
// Error : not defined :  console.log("@file 4 : test d - DELETED PARENTNAMESPACE FROM GLOBAL - log  d : " + parentNamespace.handleForChildExports + "  // undefined");
// Error : not defined : console.log("@file 4 - DELETED HANDLE - log 1   childNamespace : " + parentNamespace.childNamespace + " // [object Object]" );
console.log(" ");
//# sourceMappingURL=parentNamespace4.js.map